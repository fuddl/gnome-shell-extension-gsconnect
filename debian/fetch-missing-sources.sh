#!/bin/sh

# NOTE: Run in debian/ directory!
mkdir -p missing-sources

# keepassxc-browser/browser-polyfill.min.js
# (https://github.com/mozilla/webextension-polyfill)
curl 'https://unpkg.com/webextension-polyfill@0.6.0/dist/browser-polyfill.js' \
  > missing-sources/browser-polyfill.js
